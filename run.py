from mask_detection_engine import mask_detection_engine
from tools import time_log, stdo, time_list
import arg_check
import cv2

###########
# GLOBALS #
###########
program_arguments = None
mde = None
is_rpi = None
is_video = None


####################
# CUSTOM FUNCTIONS #
####################
def args_init():
    global program_arguments, is_rpi, is_video

    arg_check.control()
    program_arguments = arg_check.arguments

    is_rpi = True if program_arguments["camera"].lower() == "picam" else False
    is_video = True if program_arguments["stream"].lower() == "video" else False


########
# MAIN #
########
def main():
    global program_arguments, mde, is_rpi, is_video

    time_log(option = "start", id = "Mask Detection")
    args_init()
    arg_check.arg_info()

    mde = mask_detection_engine(program_arguments["mask_model_path"], program_arguments["face_model_path"], is_rpi, program_arguments["mask_threshold"], is_avx_supported = program_arguments["is_avx_supported"])

    """
    def detect_mask(self, is_video, video_frame_counter = 30, face_threshold = None, mask_threshold = None, local_file_path = None, is_gui_enabled = False, is_verbose = True):
    """

    video_frame_counter, mask_counter, best_mask_image, best_mask_value = mde.detect_mask(is_video, program_arguments["video_frame_counter"], program_arguments["mask_threshold"], program_arguments["file"], program_arguments["is_gui_enabled"], program_arguments["verbose"])

    if mask_counter == video_frame_counter / 3:
        mask_string = "Mask Detected (Rating: {})".format(best_mask_value)
    else:
        mask_string = "Mask Not detected (Rating: {})".format(best_mask_value)

    stdo(1, mask_string)

    if program_arguments["show_image"]:
        cv2.imshow(mask_string, best_mask_image)
        # waits for user to press any key
        # (this is necessary to avoid Python kernel form crashing)
        cv2.waitkey(0)
        # closing all open windows
        cv2.destroyAllWindows()

    time_log("end", id = "Mask Detection")
    stdo(1, "Time passed: {:.2f} second".format( time_list["Mask Detection"]["passed"] / 1000))
    stdo(1, "Mask Detection Program Ended.")
    return 0


if __name__ == '__main__':
    main()
