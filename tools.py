import os
import time
import random
import shutil
from glob import glob
from inspect import currentframe, getframeinfo


time_struct = {
    "start": 0,
    "end": 0,
    "passed": 0
}

time_list = {}


def stdo(flag, string = "No Info Found", frame_info = None):

    # Proper Time Parsing
    currentTime = ""
    for part in get_time().split("_"):
        if len(part) != 2:
            currentTime = currentTime + "0" + part[0] + ":"
        else:
            currentTime = currentTime + part + ":"
    currentTime = currentTime[:-1]

    if string == "No Info Found":
        print("""
[{0}] - ERR: Invalid arguments!
                       |- FLAG: {1}
                       |- FRAMEINFO: {2}
                       '- STRING: {3}\n
""".format( currentTime, flag, frame_info, string ) )

    elif flag == 1:
        print("[{0}] - INF: {1}".format( currentTime, string ) )

    elif flag == 2:
        print("[{0}] - WRN: {1}".format( currentTime, string ) )

    elif flag == 3:
        if frame_info is not None:
            print("""
[{0}] - ERR:   Invalid arguments!
                    |- PATH: {1} ({2})
                    '- MESSAGE: {3}
    """.format( currentTime, str(frame_info.filename), str(frame_info.lineno), string ) )
        else:
            print("""
[{0}] - ERR:    {1}
    """.format( currentTime, string ) )

    return 0


def get_time(level=1):
    if level == 0:  # To get raw time (For seeding random library)
        return time.time()
    if level == 1 or level == 2:
        if level == 1:  # To get only clock
            cTime = time.localtime( time.time() )[3:6]

        elif level == 2:  # To get only date
            cTime = time.localtime( time.time() )[:3]

        sCTime = ""
        for part in cTime:
            sCTime += str( part ) + "_"
        return sCTime[:-1]

    elif level == 3:  # To get date-clock (For file names)
        sCTime = "{0}-{1}".format( get_time(level=2), get_time(level=1) )

    elif level == 4:  # To get date | clock (For output logs)
        sCTime = "{0} | {1}".format( get_time(level=2), get_time(level=1) )

    else:
        return ""

    return sCTime


def get_file_name(name = "file", extension = None, with_time = True, random_seed = None, random_min_max = [0, 1024]):
    if with_time:
        if extension is not None:
            file_name = "{}_{}.{}".format(name, get_time(level = 2), extension)
        else:
            file_name = "{}_{}".format(name, get_time(level = 2))
    else:
        if random_seed is None:
            random.seed(time.time())
        else:
            random.seed(random_seed)
        if extension is not None:
            file_name = "{}_{}.{}".format(name, random.randint(random_min_max[0], random_min_max[1]), extension)
        else:
            file_name = "{}_{}".format(name, random.randint(random_min_max[0], random_min_max[1]))
    return file_name


def get_OS():
    OS = ""

    if(os.name == "nt"):
        OS = "W"    # Windows
    else:
        OS = "P"    # Posix (Linux etc...)

    return OS


def time_log(option, id = "id"):
    global time_list, time_struct

    if len(time_list) == 0:
        time_list[id] = time_struct.copy()
    elif id != "id" and id not in time_list:
        time_list[id] = time_struct.copy()

    if option == "end" and time_list[id]["start"] == 0:
        stdo(2, "Start time is not initilized. Taken time will be saved as start time.")
        option = "start"

    time_list[id][option] = time.time()  # To count program start time
    if option == "end":
        time_list[id]["passed"] = ( time_list[id]["end"] - time_list[id]["start"] ) * 1000


def list_files(path = "", name = "*", extensions = ["png"], recursive = False, verbose = True):
    # https://mkyong.com/python/python-how-to-list-all-files-in-a-directory/
    try:
        files = list()
        if recursive:
            if path[-1] != "/":
                path = path + "/"
        else:
            if path[-1] == "/":
                path = path[:-1]

        for extension in extensions:
            files.extend([f for f in glob(path + "**/{}.{}".format(name, extension), recursive = recursive)])

        """ RECURSİVE
        for path in files:
            if path.split("/")[-2] != ""
        """
        if verbose:
            output = "- {}".format(path)
            for subPath in files:
                subPath = subPath.replace(path, "")
                output += "\n"
                for i in range(len(path.split("/"))):
                    output += "\t"
                output += "\t|- {}".format(subPath)
            stdo(1, output)
            stdo(1, "{} files found".format( len(files) ))

        return files

    except Exception as error:
        stdo(3, "An error occured while working in fileList function -> " + error.__str__(),  getframeinfo(currentframe() ) )
        stdo(1, "--- --- ---")
    return None


def name_parsing(filePath, separate = False, separator = ".", maxSplit = -1):    # parsing name from file path
    file = filePath.split("/")[-1]
    extension = file.split(".")[-1]
    name = file.strip( "." + extension)
    if separate:
        # https://www.programiz.com/python-programming/methods/string/strip
        name = name.split(separator, maxSplit)
    return name, extension


def clear_dir(dir_path, ignore_errors = False):
    stdo(1, string = "'{}' path deleting...".format(dir_path))
    try:
        shutil.rmtree(dir_path, ignore_errors, onerror = None)
        stdo(1, string = "'{}' path deleted".format(dir_path))

    except FileNotFoundError:
        stdo(1, string = "'{}' path is not available".format(dir_path))

    except OSError as error:
        stdo(3, "Error Occured while working in 'clear_dir': {}".format( error.__str__() ),  getframeinfo( currentframe() ))
        exit(-1)
