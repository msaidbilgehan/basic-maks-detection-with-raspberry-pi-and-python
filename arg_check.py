# Unexpected Bool behaivior at argparse. Do not take user parameter as true or false and stucks at default value
#       https://bugs.python.org/issue26994
#       https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse

###########
# IMPORTS #
###########
from tools import stdo    # For properly print out
from inspect import currentframe, getframeinfo
import argparse


###########
# GLOBALS #
###########
arguments = None
arguments_output = ""


####################
# CUSTOM FUNCTIONS #
####################
# https://stackoverflow.com/questions/15753701/how-can-i-pass-a-list-as-a-command-line-argument-with-argparse
def control():
    global arguments

    arg_parser = argparse.ArgumentParser()  # Parse arguments

    arg_parser.add_argument("-mm", "--mask-model-path", default = "models/mask_detector/mask_detector.model",
                    help = "The path for mask detection knn model (model file path - default: models/mask_detector/mask_detector.model)")

    arg_parser.add_argument("-fm", "--face-model-path", default = "models/face_detector/",
                    help = "The path for face detection model files (file path - default: models/face_detector/)")

    arg_parser.add_argument("-cam", "--camera", default = "picam",
                    help = "NOT NECCESSARY - specifiy if image will be taken by piCamera with picam or PC camera with cam (cam/picam), anything else will be default option which is local image (default: picam)")

    arg_parser.add_argument("-s", "--stream", default = "video",
                    help = "If it is stream or brust of images (video or image - default: video)")

    arg_parser.add_argument("-f", "--file", default = None,
                    help = "If it is from a local file (A file path - default: None)")

    arg_parser.add_argument("-vfc", "--video-frame-counter", type=int, default = 32,
                    help = "Stream Image Number or Brust of Image Number (1 to any integer - default: 32)")

    arg_parser.add_argument("-mt", "--mask-threshold", type=float, default = 0.7,
                    help = "Threshold value of Sucsesfull Mask Detection (0 to 1 - default: 0.7)")

    arg_parser.add_argument("-si", "--show-image", type=str_to_bool, default = False,
                    help = "Show Best Mask Image with default CV imshow function (True or False - default: True)")

    arg_parser.add_argument("-avx", "--is-avx-supported", type=str_to_bool, default = True,
                    help = "Older CPUs don't support AVX, thus Tensorflow version will be changed from latest to 1.5.0 (True or False - default: True)")

    arg_parser.add_argument("-gui", "--is-gui-enabled", type=str_to_bool, default = False,
                    help = "To see video or photo frame with OpenCV Window (True or False - default: False)")

    arg_parser.add_argument("-v", "--verbose", type=str_to_bool, default = False,
                    help = "More detailed outputs to terminal (True or False - default: False)")

    arguments = vars(arg_parser.parse_args())
    return 0


def output_args(arguments, only_cache = True):
    # https://docs.python.org/3/tutorial/datastructures.html
    # https://stackoverflow.com/questions/9453820/alternative-to-python-string-item-assignment

    try:
        global arguments_output
        # imageName = args["image"].split("/")[-1]

        arguments_output = "Given Arguments:\n"
        for key in list(arguments):
            arguments_output += "\t\t    |- {} : {}\n".format(key, arguments[key])

        list_of_args = list(arguments_output)
        index = arguments_output.rfind("|")
        if index is not -1:
            list_of_args[ index ] = "'"

            arguments_output = ""
            for char in list_of_args:
                arguments_output += char

            if not only_cache:
                stdo(1, arguments_output)
            return 0
        else:
            return index
    except Exception as error:
        stdo(3, "An error occured while output_args function runtime in main run.py -> " + error.__str__(),  getframeinfo(currentframe() ) )
        return -1


def arg_info(need_update = False):
    global arguments_output, arguments
    if arguments_output == "" or need_update:
        output_args(arguments, False)
    else:
        stdo(1, arguments_output)
    return 0


def str_to_bool(str):
    if isinstance(str, bool):
        return str
    if str.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif str.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
