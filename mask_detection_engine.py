# REFERENCES
#   Threading
#       https://pymotw.com/3/threading/
#       https://realpython.com/intro-to-python-threading/
#       https://docs.python.org/3/library/concurrent.futures.html
#       https://stackoverflow.com/questions/6893968/how-to-get-the-return-value-from-a-thread-in-python
#       https://stackoverflow.com/questions/3221655/python-threading-string-arguments
#       https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
#       https://medium.com/school-of-code/classmethod-vs-staticmethod-in-python-8fe63efb1797
#   Global import in function namespace
#       https://stackoverflow.com/questions/11990556/how-to-make-global-imports-from-a-function
#   Using Video Frames in Rpi Camera and Python API
#       https://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/
#   Using Picam with Raspberry Pi
#       https://picamera.readthedocs.io/en/latest/recipes2.html#rapid-capture-and-processing
#   Using Webcam with OpenCV
#       https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html
#   Tensorflow Dependincies Issue
#       https://tech.amikelive.com/node-887/how-to-resolve-error-illegal-instruction-core-dumped-when-running-import-tensorflow-in-a-python-program/
#       https://stackoverflow.com/questions/49092527/illegal-instructioncore-dumped-tensorflow
#       https://stackoverflow.com/questions/48720833/could-not-find-a-version-that-satisfies-the-requirement-tensorflow


# Currently, we are using threading with nested threadpoolexecutor due to the instability of asyncio (still developing and changing APIs...)
# https://stackoverflow.com/questions/44989473/nesting-concurrent-futures-threadpoolexecutor


# ######### #
# LIBRARIES #
# ######### #
import cv2
# import dlib
# import threading
import numpy as np
from time import sleep
# from enum import Enum
from os.path import isfile  # , abspath
from os import path
from tools import stdo, get_file_name
# from concurrent.futures import ThreadPoolExecutor


# ####### #
# CLASSES #
# ####### #
"""
class Statu(Enum):
    YES_MASK = 0
    NO_MASK = 1
    CHECKING_MASK = 2
"""


# ##################### #
# Object Initialization #
# ##################### #
class mask_detection_engine(object):
    def __init__(self, mask_model_path = None, face_model_path = None, is_rpi = False, face_threshold = 0.7, mask_threshold = 0.7, camera_resolution = (400, 400), is_avx_supported = True):
        self.is_rpi = is_rpi
        self.mask_model_path = mask_model_path
        self.face_model_path = face_model_path
        self.mask_threshold = mask_threshold
        self.face_threshold = face_threshold
        self.camera_resolution = camera_resolution
        self.model = None
        self.camera = None
        self.PiRGBArray = None
        self.raw_capture = None
        self.video_frame_counter = 30
        self.mask_counter = 0
        self.best_mask_value = 0
        self.best_mask_image = None
        self.is_avx_supported = is_avx_supported

        self.face_net = None
        self.prototxt_path = None
        self.weights_path = None
        self.mask_net = None
        self.last_faces = []
        self.last_locs = []
        self.last_preds = []

        # self.g_lock = threading.Lock()
        # TODO - Add MLX90614 tempreture check
        # self.current_celcius_tempreture = None

        global load_model, preprocess_input, img_to_array
        try:
            import warnings
            with warnings.catch_warnings():
                warnings.filterwarnings("ignore", category = FutureWarning)
                if self.is_avx_supported:
                    # Latest tensorflow support
                    from tensorflow.keras.models import load_model
                    from tensorflow.keras.applications.mobilenet import preprocess_input
                    from tensorflow.keras.preprocessing.image import img_to_array
                else:
                    from tensorflow.python.keras.models import load_model
                    from tensorflow.python.keras.applications.mobilenet import preprocess_input
                    from tensorflow.python.keras.preprocessing.image import img_to_array
        except Exception:
            stdo(3, "Be sure the tensorflow versions correct (For CPUs which unsupport AVX feature, use 1.5.0 version of Tensorflow)")
            exit(-1)

        if self.mask_model_path is not None:
            self.initilizing()

    def __del__(self):
        if not self.is_rpi:
            self.camera.release()

    def initilizing(self):
        try:
            if isfile(self.mask_model_path):
                # self.mask_model_path = abspath(self.mask_model_path)
                self.prototxt_path = path.sep.join([self.face_model_path, "deploy.prototxt"])
                self.weights_path = path.sep.join([self.face_model_path,
                    "res10_300x300_ssd_iter_140000.caffemodel"])
                self.face_net = cv2.dnn.readNet(self.prototxt_path, self.weights_path)
                self.mask_net = load_model(self.mask_model_path)
            else:
                stdo(3, "File is not exist. PATH: {}".format(self.mask_model_path))

        except Exception:
            stdo(3, "An error Occured while initilizing Mask Detection Engine mask_model_path:{}\tface_model_path:{}\n\tError Details:{}".format(self.mask_model_path, self.face_model_path, Exception.__str__))

        if self.is_rpi:
            self.rpi_configurations()
        """
        # WebCam configurations will be done at detect_mask run time iwth local_file_path parameter control
        else:
            self.webcam_configurations()
        """

    def rpi_configurations(self):
        try:
            # global PiRGBArray, PiCamera
            from picamera.array import PiRGBArray
            from picamera import PiCamera

            self.camera = PiCamera()
            self.PiRGBArray = PiRGBArray()

            self.camera.framerate = 32
            self.camera.resolution = self.camera_resolution
            self.camera.rotation = 180
            self.raw_capture = self.PiRGBArray(self.camera, size = self.camera_resolution)

            sleep(0.1)  # allow the camera to warmup

        except ImportError as i_err:
            stdo(3, "RPi Camera (picamera) library error! Error Detail: {}".format(i_err.__str__()))

    def webcam_configurations(self, local_file_path):
        try:
            # from VideoCapture import Device
            if local_file_path is not None:
                if isfile(local_file_path):
                    # local_file_path = abspath(local_file_path)
                    self.camera = cv2.VideoCapture(local_file_path)
                else:
                    stdo(3, "Video File is not Found! Path: {}".format(local_file_path))
                    exit(-1)
            else:
                self.camera = cv2.VideoCapture(0)

            if not self.camera.isOpened():
                self.camera.open()

            self.camera.set(3, self.camera_resolution[0])
            self.camera.set(4, self.camera_resolution[1])

        except Exception:
            stdo(3, "An error occured at webcam_configurations()! Error Detail: {}".format(Exception.__str__))
            exit(-1)

    def _detector_mask(self, image):
        copy_img = image.copy()

        resized = cv2.resize(copy_img, (254, 254))
        resized = img_to_array(resized)
        resized = preprocess_input(resized)
        resized = np.expand_dims(resized, axis=0)

        return self.model.predict([resized])[0]

    def detect_mask(self, is_video, video_frame_counter = 30, face_threshold = None, mask_threshold = None, local_file_path = None, is_gui_enabled = False, is_verbose = True):
        self.mask_counter = 0
        self.best_mask_value = 0
        self.best_mask_image = None

        if mask_threshold is None:
            mask_threshold = self.mask_threshold

        if face_threshold is None:
            face_threshold = self.face_threshold

        if is_video:
            self.video_frame_counter = video_frame_counter

            if self.is_rpi:
                ret, frame = True, self.camera.capture_continuous(self.raw_capture, format="bgr", use_video_port=True)
            else:
                if local_file_path is not None:
                    self.webcam_configurations(local_file_path)
                else:
                    self.webcam_configurations()

            for frame in range(0, video_frame_counter, 1):

                ret, frame = self.camera.read()

                # Our operations on the frame come here
                # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # grab the dimensions of the frame and then construct a blob
                # from it
                (h, w) = frame.shape[:2]
                blob = cv2.dnn.blobFromImage(frame, 1.0, (300, 300),
                    (104.0, 177.0, 123.0))
                # pass the blob through the network and obtain the face detections
                self.face_net.setInput(blob)
                face_detections = self.face_net.forward()
                # initialize our list of faces, their corresponding locations,
                # and the list of predictions from our face mask network
                self.last_faces = []
                self.last_locs = []
                self.last_preds = []

                # loop over the face_detections
                for i in range(0, face_detections.shape[2]):
                    # extract the confidence (i.e., probability) associated with
                    # the detection
                    confidence = face_detections[0, 0, i, 2]
                    # filter out weak face_detections by ensuring the confidence is
                    # greater than the minimum confidence
                    if confidence > mask_threshold:
                        # compute the (x, y)-coordinates of the bounding box for
                        # the object
                        box = face_detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                        (startX, startY, endX, endY) = box.astype("int")

                        # ensure the bounding boxes fall within the dimensions of
                        # the frame
                        (startX, startY) = (max(0, startX), max(0, startY))
                        (endX, endY) = (min(w - 1, endX), min(h - 1, endY))

                        # extract the face ROI, convert it from BGR to RGB channel
                        # ordering, resize it to 224x224, and preprocess it
                        face = frame[startY:endY, startX:endX]
                        face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
                        face = cv2.resize(face, (224, 224))
                        face = img_to_array(face)
                        face = preprocess_input(face)

                        # add the face and bounding boxes to their respective
                        # lists
                        self.last_faces.append(face)
                        self.last_locs.append((startX, startY, endX, endY))

                # only make a predictions if at least one face was detected
                if len(self.last_faces) > 0:
                    # for faster inference we'll make batch predictions on *all*
                    # faces at the same time rather than one-by-one predictions
                    # in the above `for` loop
                    self.last_faces = np.array(self.last_faces, dtype="float32")
                    self.last_preds = self.mask_net.predict(self.last_faces, batch_size=32)

                if is_gui_enabled:
                    for (box, pred) in zip(self.last_faces, self.last_preds):
                        # unpack the bounding box and predictions
                        (startX, startY, endX, endY) = box
                        (mask, withoutMask) = pred

                        # determine the class label and color we'll use to draw
                        # the bounding box and text
                        label = "Mask" if mask > withoutMask else "No Mask"
                        color = (0, 255, 0) if label == "Mask" else (0, 0, 255)

                        # include the probability in the label
                        label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)

                        # display the label and bounding box rectangle on the output
                        # frame
                        cv2.putText(frame, label, (startX, startY - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)
                        cv2.rectangle(frame, (startX, startY), (endX, endY), color, 2)

                    # show the output frame
                    cv2.imshow("Frame", frame)
                    #key = cv2.waitKey(1) & 0xFF

                    # if the `q` key was pressed, break from the loop
                    #if key == ord("q"):
                    #    break

            if is_gui_enabled:
                # do a bit of cleanup
                cv2.destroyAllWindows()

            # return a 2-tuple of the face locations and their corresponding
            # locations
            return (self.last_locs, self.last_preds)

            """
            if faces > 0:
                mask, _ = self._detector_mask(image)

                if mask > mask_threshold:
                    self.mask_counter += 1

                if mask > self.last_mask_value:
                    self.best_mask_value = mask
                    self.best_mask_image = image

                if is_verbose:
                    stdo(1, "Current frame mask value: {}".format(mask))

                if self.is_rpi:
                    # clear the stream in preparation for the next frame
                    self.raw_capture.truncate(0)

                if(self.video_frame_counter == 0):
                    break
                else:
                    self.video_frame_counter -= 1
            else:
                stdo(3, "No face detected.")
            """

        else:
            self.camera.saveSnapshot(get_file_name("current_image", ".jpg"))
            stdo(2, "This part is at development. Please contact developer <msaidbilgehan@gmail.com>")
            exit(-2)

        if is_verbose:
            stdo(1, "Counted Mask(s) Number at Camera ({} frame): {}".format(self.video_frame_counter, self.mask_counter))
        return self.video_frame_counter, self.mask_counter, self.best_mask_image, self.best_mask_value

# ####### #
# GLOBALS #
# ####### #
